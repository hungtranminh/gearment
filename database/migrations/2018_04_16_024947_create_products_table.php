<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('image');
            $table->float('price');
            $table->float('recommend_price');
            $table->float('tax');
            $table->string('sku');
            $table->float('weight');
            $table->integer('type_id');
            $table->string('vendor');
            $table->integer('collection_id');
            $table->string('tags');
            $table->string('hs_code');
            $table->integer('resources_details_id');
            $table->string('page_title');
            $table->string('meta_description');
            $table->string('link');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
