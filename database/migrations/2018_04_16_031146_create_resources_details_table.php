<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resources_info_id');
            $table->string('color');
            $table->string('color_hex');
            $table->integer('size');
            $table->float('extra_charge');
            $table->float('weight');
            $table->float('buy_price');
            $table->float('base_cost');
            $table->integer('available');
            $table->integer('on_hand');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources_details');
    }
}
