<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type_id');
            $table->integer('material_id');
            $table->integer('brand_id');
            $table->string('sku');
            $table->string('description');
            $table->string('color');
            $table->string('size');
            $table->string('front_img');
            $table->string('back_img');
            $table->string('design_front');
            $table->string('design_back');
            $table->integer('available');
            $table->integer('on_hand');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources_info');
    }
}
