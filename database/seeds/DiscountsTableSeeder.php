<?php

use Illuminate\Database\Seeder;
use App\Models\Discount;

class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        $limit = 4;

        for ($i = 0; $i < $limit; $i++) {
            Discount::create([
                'name' => $faker->name(),
                'discount' => $faker->numberBetween(3,15),
                'from' => $faker->dateTime(),
                'to' => $faker->dateTime(),
            ]);
        }
    }
}
