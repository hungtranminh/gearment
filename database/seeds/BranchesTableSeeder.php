<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        $limit = 4;

        for ($i = 0; $i < $limit; $i++) {
            Brand::create([
                'name' => $faker->streetName(),
                'description' => $faker->text(),
            ]);
        }
    }
}
