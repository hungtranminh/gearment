<?php

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('en_US');

        $limit = 4;

        for ($i = 0; $i < $limit; $i++) {
            Type::create([
                'name' => $faker->streetName(),
                'description' => $faker->text(),
            ]);
        }
    }
}
