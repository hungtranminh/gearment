<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        $limit = 4;
        $dob = $faker->dateTime('now');
//        $dob => date_format($dob, "d-m-Y h-i-s");

        for ($i = 0; $i < $limit; $i++) {
            User::create([
                'name' => $faker->name(50),
                'email' => $faker->unique()->email,
                'password' => bcrypt('123456'),
                'dob' => date_format($dob, "Y-m-d"),
                'gender' => $faker->numberBetween(0, 1),
                'phone' => random_int(1000000000, 1111111111),
                'role' => random_int(1,3),
                'status' => random_int(0,1),
            ]);
        }
    }
}